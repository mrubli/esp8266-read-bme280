/*
 * BME280 Temperature/humidity sensor read-out
 *
 * Pin assignment:
 * BME280   NodeMCU
 * SCL      D1
 * SDA      D2
 */

#include <Arduino.h>
#include <Wire.h>


#define PRINT_RAW_DATA 1


namespace
{

    const auto I2cAddress = 0x76;   // 0x76/0x77 if SDO pulled low/high

}


void setup()
{
    Serial.begin(9600);

#if 1
    // Initialize I2C with default pin parameters
    Wire.begin();   // Pins 4 (D2 = SDA) and 5 (D1 = SCL)
#else
    // Use this for custom setups (these values are for the NodeMCU board)
    static_assert(D1 == 5, "D1 ouch");
    static_assert(D2 == 4, "D2 ouch");
    static_assert(SDA == D2, "SDA ouch");
    static_assert(SCL == D1, "SCL ouch");
    Wire.begin(SDA, SCL);
#endif
}


bool
read_reg(uint8_t reg, uint8_t& data)
{
    // Write the register byte
    Wire.beginTransmission(I2cAddress);
    Wire.write(reg);
    const auto res = Wire.endTransmission();
    if(res != 0)
    {
        Serial.print("ERROR: I2C write failed: ");
        Serial.print(res);
        Serial.println();
        return false;
    }

    // Read the reply
    Wire.requestFrom(I2cAddress, sizeof(data));
    auto offset = 0u;
#if PRINT_RAW_DATA
    Serial.print("Received:");
#endif
    uint8_t inBuffer[1] = {};
    while(offset < sizeof(inBuffer) && Wire.available() > 0)
    {
        inBuffer[offset] = Wire.read();
#if PRINT_RAW_DATA
        Serial.print(" 0x");
        Serial.print(inBuffer[offset], HEX);
#endif
        offset++;
    }
#if PRINT_RAW_DATA
    Serial.println();
#endif
    if(offset < sizeof(inBuffer))
    {
        Serial.print("ERROR: Short I2C read (");
        Serial.print(offset);
        Serial.println(" bytes, expected: ");
        Serial.print(sizeof(inBuffer));
        Serial.println(" bytes)");
        return false;
    }

    data = inBuffer[0];
    return true;
}


void loop()
{
    delay(1000);

    // Read the chip ID
    {
        uint8_t data = 0;
        if(read_reg(0xD0, data))
        {
            Serial.print("Chip ID: 0x");
            Serial.print(data, HEX);
            Serial.println();
        }
        else
        {
            Serial.print("ERROR: Failed to read chip ID.");
            return;
        }
    }

}
